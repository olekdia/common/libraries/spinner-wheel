/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.spinnerwheel;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.olekdia.spinnerwheel.adapters.AbstractWheelAdapter;
import com.olekdia.spinnerwheel.adapters.ArrayWheelAddableAdapter;
import com.olekdia.spinnerwheel.adapters.ArrayWheelAddableInIntervalAdapter;

import java.util.LinkedList;
import java.util.List;

/**
 * Abstract spinner spinnerwheel view.
 * This class should be subclassed.
 *
 * @author Yuri Kanivets
 * @author Dimitri Fedorov
 */
public abstract class AbstractWheel extends FrameLayout {

    //----------------------------------
    //  Default properties values
    //----------------------------------

    /**
     * Default count of visible items
     */
    private static final int DEF_VISIBLE_ITEMS = 4;
    private static final boolean DEF_IS_CYCLIC = false;
    private static final float FLING_STOP_DIST_REDUCTION = 0.75F;

    //----------------------------------
    //  Class properties
    //----------------------------------

    protected int mCurrentItemIdx = 0;

    // Count of visible items
    protected int mVisibleItems;
    // Should all items be visible
    protected boolean mIsAllVisible;

    protected boolean mIsCyclic;

    // Scrolling
    protected WheelScroller mScroller;
    protected boolean mIsScrollingPerformed;
    protected int mScrollingOffset;

    // Items layout
    protected LinearLayout mItemsLayout;

    // The number of first item in layout
    protected int mFirstItemIdx;

    protected ItemsRange mItemsRange;
    protected int[] mSelectorShaderColors;
    protected float[] mSelectorShaderPositions;

    // View adapter
    protected AbstractWheelAdapter mViewAdapter;

    protected int mLayoutHeight;
    protected int mLayoutWidth;

    // Recycle
    private WheelRecycler mRecycler = new WheelRecycler(this);

    // Listeners
    private OnWheelChangeListener mOnChangeListener;
    private OnWheelClickListener mOnClickListener;
    private OnWheelInsertListener mOnInsertListener;
    private List<OnWheelScrollListener> mScrollingListeners = new LinkedList<OnWheelScrollListener>();

    //XXX: I don't like listeners the way as they are now. -df

    // Adapter listener
    private DataSetObserver mDataObserver;

    protected EditText mInputField;

    private boolean mIsInputEnabled;
    private boolean mIsResetInputByFocusLose;

    /**
     * The numbers accepted by the input text's {@link LayoutInflater.Filter}
     */
    private static final char[] DIGIT_CHARACTERS = new char[]{
            // Latin digits are the common case
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            // Eastern-Arabic
            '٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩',
            // Perso-Arabic
            '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹',
            // Gujarati
            '૦', '૧', '૨', '૩', '૪', '૫', '૬', '૭', '૮', '૯',
            // Hindi and Marathi (Devanagari script)
            '०', '१', '२', '३', '४', '५', '६', '७', '८', '९',
            // Thai
            '๐', '๑', '๒', '๓', '๔', '๕', '๖', '๗', '๘', '๙',
            // Bengali
            '\u09e6', '\u09e7', '\u09e8', '\u09e9', '\u09ea', '\u09eb', '\u09ec', '\u09ed', '\u09ee', '\u09ef',
            // Kannada
            '\u0ce6', '\u0ce7', '\u0ce8', '\u0ce9', '\u0cea', '\u0ceb', '\u0cec', '\u0ced', '\u0cee', '\u0cef'
    };

    //----------------------------------------------------------------------------------------------
    //  Constructor
    //----------------------------------------------------------------------------------------------

    /**
     * Create a new AbstractWheel instance
     *
     * @param context  the application environment.
     * @param attrs    a collection of attributes.
     * @param defStyle The default style to apply to this view.
     */
    public AbstractWheel(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs);
        initAttributes(attrs, defStyle);
        initData(context);

        setWillNotDraw(false);

        addInputField();

        setIsInputEnabled(true);
    }

    //----------------------------------------------------------------------------------------------
    // Work with edit text
    //----------------------------------------------------------------------------------

    public void addInputField() {
        mInputField = new EditText(this.getContext());

        mInputField.setId(R.id.wheel_input);
        mInputField.setBackgroundColor(Color.TRANSPARENT);

        mInputField.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    mInputField.setText(mViewAdapter.getItemText(mCurrentItemIdx));
                    mInputField.selectAll();
                } else {
                    if (!mIsResetInputByFocusLose) {
                        changeCurrPosByInputValue();
                    }
                    hideSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
            }
        });
        mInputField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    changeCurrPosByInputValue();
                    hideSoftInput(0);
                }
                return false;
            }
        });

        mInputField.setImeOptions(EditorInfo.IME_ACTION_DONE
                | EditorInfo.IME_FLAG_NO_FULLSCREEN
                | EditorInfo.IME_FLAG_NO_EXTRACT_UI);

        mInputField.setGravity(Gravity.CENTER);
        mInputField.setMaxLines(2);

        addView(mInputField, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

        mInputField.setVisibility(View.GONE);
    }

    public final EditText getInputField() {
        return mInputField;
    }

    private void showSoftInput() {
        if (!mIsInputEnabled) return;

        final InputMethodManager im = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (im != null) {
            if (mItemsLayout != null && mItemsLayout.getChildCount() > 1) {
                mItemsLayout.getChildAt(mItemsLayout.getChildCount() - 2
                                                + (mViewAdapter.isLastItem(mCurrentItemIdx) && !isCyclic() ? 1 : 0))
                            .setWillNotDraw(true);
            }
            mInputField.setVisibility(View.VISIBLE);
            mInputField.requestFocus();
            im.showSoftInput(mInputField, 0);
        }
    }

    public final void cancelInput() {
        hideSoftInput(0);
    }

    private void hideSoftInput(final int hideFlag) {
        final InputMethodManager im = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (im != null && im.isActive(mInputField)) {
            if (mItemsLayout != null && mItemsLayout.getChildCount() > 0) {
                for (int i = 0; i < mItemsLayout.getChildCount(); i++) {
                    mItemsLayout.getChildAt(i).setWillNotDraw(false);
                }
            }
            im.hideSoftInputFromWindow(getWindowToken(), hideFlag);
            mInputField.setVisibility(View.GONE);
        }
    }

    private void changeCurrPosByInputValue() {
        int toPos = mViewAdapter.findPosInputValue(this);
        invalidateItemsLayout(true);
        if (toPos != -1) {
            if (!isValidItemIndex(toPos)) toPos = 0;

            // Ugly solution to notify listeners that value changed,
            // but actually index of value the same because new value is inserted before previous selected
            if (toPos == mCurrentItemIdx &&
                    (mViewAdapter instanceof ArrayWheelAddableInIntervalAdapter
                            || mViewAdapter instanceof ArrayWheelAddableAdapter)) {
                notifyChangeListener(toPos, mCurrentItemIdx);
            }
            setCurrentItem(toPos, false);
        }
    }

    /**
     * @return The selected index given its displayed <code>value</code>.
     */
    private int getSelectedPos(final String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            // Ignore as if it's not a number we don't care
        }
        return mViewAdapter.getMinValue();
    }

    /**
     * Filter for accepting only valid indices or prefixes of the string
     * representation of valid indices.
     */
    public class InputTextFilter extends NumberKeyListener {

        // XXX This doesn't allow for range limits when controlled by a
        // soft input method!
        public int getInputType() {
            return InputType.TYPE_CLASS_TEXT;
        }

        @Override
        protected char[] getAcceptedChars() {
            return DIGIT_CHARACTERS;
        }

        @Override
        public CharSequence filter(
                CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            CharSequence filtered = super.filter(source, start, end, dest, dstart, dend);

            if (filtered == null) filtered = source.subSequence(start, end);

            final String result = String.valueOf(dest.subSequence(0, dstart)) + filtered
                    + dest.subSequence(dend, dest.length());

            if ("".equals(result)) return result;

            final int val = getSelectedPos(result);

                /*
                 * Ensure the user can't type in a value greater than the max
                 * allowed. We have to allow less than min as the user might
                 * want to delete some numbers and then type a new number.
                 * And prevent multiple-"0" that exceeds the length of upper
                 * bound number.
                 */
            if (val > mViewAdapter.getMaxValue() || result.length() > String.valueOf(mViewAdapter.getMaxValue()).length()) {
                return "";
            } else {
                return filtered;
            }
        }
    }
    //----------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    //
    //  Initiating data and assets at start up
    //
    //--------------------------------------------------------------------------

    /**
     * Initiates data and parameters from styles
     *
     * @param attrs    a collection of attributes.
     * @param defStyle The default style to apply to this view.
     */
    protected void initAttributes(final AttributeSet attrs, final int defStyle) {
        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.AbstractWheelView, defStyle, 0);
        mVisibleItems = a.getInt(R.styleable.AbstractWheelView_visibleItems, DEF_VISIBLE_ITEMS);
        mIsAllVisible = a.getBoolean(R.styleable.AbstractWheelView_isAllVisible, false);
        mIsCyclic = a.getBoolean(R.styleable.AbstractWheelView_isCyclic, DEF_IS_CYCLIC);
        a.recycle();
    }

    /**
     * Initiates data
     *
     * @param context the mContext
     */
    protected void initData(final Context context) {

        mItemsRange = new ItemsRange();

        if (mVisibleItems == 2) {
            mSelectorShaderColors = new int[6];
            mSelectorShaderColors[2] = 0xFF000000;
            mSelectorShaderColors[3] = 0xFF000000;

            mSelectorShaderPositions = new float[6];
            mSelectorShaderPositions[0] = 0;
            mSelectorShaderPositions[5] = 1;
        } else {
            mSelectorShaderColors = new int[10];
            mSelectorShaderColors[0] = 0;
            mSelectorShaderColors[4] = 0xFF000000;
            mSelectorShaderColors[5] = 0xFF000000;
            mSelectorShaderColors[9] = 0;

            mSelectorShaderPositions = new float[10];
            mSelectorShaderPositions[0] = 0;
            mSelectorShaderPositions[9] = 1;
        }

        mDataObserver = new DataSetObserver() {
            @Override
            public void onChanged() {
                invalidateItemsLayout(false);
            }

            @Override
            public void onInvalidated() {
                invalidateItemsLayout(true);
            }
        };

        // creating new mScroller
        mScroller = createScroller(new WheelScroller.ScrollingListener() {

            public void onStarted() {
                mIsScrollingPerformed = true;
                notifyScrollingListenersAboutStart();
                onScrollStarted();
            }

            public void onTouch() {
                onScrollTouched();
            }

            public void onTouchUp() {
                if (!mIsScrollingPerformed)
                    onScrollTouchedUp(); // if scrolling IS performed, whe should use onFinished instead
            }

            public void onScroll(int distance) {
                doScroll(distance);

                int dimension = (int) (getBaseDimension() * FLING_STOP_DIST_REDUCTION);
                if (mScrollingOffset > dimension) {
                    mScrollingOffset = dimension;
                    mScroller.stopScrolling();
                } else if (mScrollingOffset < -dimension) {
                    mScrollingOffset = -dimension;
                    mScroller.stopScrolling();
                }
            }

            public void onFinished() {
                if (mIsScrollingPerformed) {
                    notifyScrollingListenersAboutEnd();
                    mIsScrollingPerformed = false;
                    onScrollFinished();
                }

                mScrollingOffset = 0;
                invalidate();
            }

            public void onJustify() {
                if (Math.abs(mScrollingOffset) > WheelScroller.MIN_DELTA_FOR_SCROLLING) {
                    mScroller.scroll(mScrollingOffset, 0);
                }
            }
        });
    }

    @Override
    public Parcelable onSaveInstanceState() {
        //begin boilerplate code that allows parent classes to save state
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        //end

        ss.mCurrentItem = getCurrentItem();

        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        //begin boilerplate code so parent classes can restore state
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        final SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        //end

        mCurrentItemIdx = ss.mCurrentItem;

        // dirty hack to re-draw child items correctly
        postDelayed(new Runnable() {
            @Override
            public void run() {
                invalidateItemsLayout(false);
            }
        }, 100);
    }

    static class SavedState extends BaseSavedState {
        int mCurrentItem;

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);

            mCurrentItem = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);

            out.writeInt(mCurrentItem);
        }

        //required field that makes Parcelables from a Parcel
        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

    abstract protected void recreateAssets(int width, int height);


    //--------------------------------------------------------------------------
    //
    //  Scroller operations
    //
    //--------------------------------------------------------------------------

    /**
     * Creates mScroller appropriate for specific wheel implementation.
     *
     * @param scrollingListener listener to be passed to the mScroller
     * @return Initialized mScroller to be used
     */
    protected abstract WheelScroller createScroller(WheelScroller.ScrollingListener scrollingListener);

    /* These methods are not abstract, as we may want to override only some of them */
    protected void onScrollStarted() {
    }

    protected void onScrollTouched() {
    }

    protected void onScrollTouchedUp() {
    }

    protected void onScrollFinished() {
    }

    /**
     * Stops scrolling
     */
    public void stopScrolling() {
        mScroller.stopScrolling();
    }

    /**
     * Set the the specified scrolling interpolator
     *
     * @param interpolator the interpolator
     */
    public void setInterpolator(final Interpolator interpolator) {
        mScroller.setInterpolator(interpolator);
    }

    /**
     * Scroll the spinnerwheel
     *
     * @param itemsToScroll items to scroll
     * @param time          scrolling duration
     */
    public void scroll(final int itemsToScroll, final int time) {
        int distance = itemsToScroll * getItemDimension() - mScrollingOffset;
        onScrollTouched(); // we have to emulate touch when scrolling spinnerwheel programmatically to light up stuff
        mScroller.scroll(distance, time);
    }

    /**
     * Scrolls the spinnerwheel
     *
     * @param delta the scrolling value
     */
    private void doScroll(final int delta) {
        hideSoftInput(0);
        mScrollingOffset += delta;

        final int itemDimension = getItemDimension();
        int count = mScrollingOffset / itemDimension;

        int pos = mCurrentItemIdx - count;
        final int itemCount = mViewAdapter.getItemsCount();

        int fixPos = mScrollingOffset % itemDimension;
        if (Math.abs(fixPos) <= itemDimension / 2) {
            fixPos = 0;
        }
        if (mIsCyclic && itemCount > 0) {
            if (fixPos > 0) {
                pos--;
                count++;
            } else if (fixPos < 0) {
                pos++;
                count--;
            }
            // fix position by rotating
            while (pos < 0) {
                pos += itemCount;
            }
            pos %= itemCount;
        } else {
            if (pos < 0) {
                count = mCurrentItemIdx;
                pos = 0;
            } else if (pos >= itemCount) {
                count = mCurrentItemIdx - itemCount + 1;
                pos = itemCount - 1;
            } else if (pos > 0 && fixPos > 0) {
                pos--;
                count++;
            } else if (pos < itemCount - 1 && fixPos < 0) {
                pos++;
                count--;
            }
        }

        int offset = mScrollingOffset;
        if (pos != mCurrentItemIdx) {
            setCurrentItem(pos, false);
        } else {
            invalidate();
        }

        // update offset
        int baseDimension = getBaseDimension();
        mScrollingOffset = offset - count * itemDimension;
        if (mScrollingOffset > baseDimension) {
            mScrollingOffset = mScrollingOffset % baseDimension + baseDimension;
        }
    }

    //--------------------------------------------------------------------------
    //
    //  Base measurements
    //
    //--------------------------------------------------------------------------

    /**
     * Returns base dimension of the spinnerwheel — width for horizontal spinnerwheel, height for vertical
     *
     * @return width or height of the spinnerwheel
     */
    abstract protected int getBaseDimension();

    /**
     * Returns base dimension of base item — width for horizontal spinnerwheel, height for vertical
     *
     * @return width or height of base item
     */
    abstract protected int getItemDimension();

    /**
     * Processes MotionEvent and returns relevant position — x for horizontal spinnerwheel, y for vertical
     *
     * @param event MotionEvent to be processed
     * @return relevant position of the MotionEvent
     */
    abstract protected float getMotionEventPosition(MotionEvent event);


    //--------------------------------------------------------------------------
    //
    //  Layout creation and measurement operations
    //
    //--------------------------------------------------------------------------

    /**
     * Creates item layouts if necessary
     */
    abstract protected void createItemsLayout();

    /**
     * Sets layout width and height
     */
    abstract protected void doItemsLayout();


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed) {
            int w = r - l;
            int h = b - t;
            doItemsLayout();
            if (mLayoutWidth != w || mLayoutHeight != h) {
                recreateAssets(getMeasuredWidth(), getMeasuredHeight());
            }
            mLayoutWidth = w;
            mLayoutHeight = h;
        }
    }

    /**
     * Invalidates items layout
     *
     * @param clearCaches if true then cached views will be cleared
     */
    public void invalidateItemsLayout(final boolean clearCaches) {
        if (clearCaches) {
            mRecycler.clearAll();
            if (mItemsLayout != null) mItemsLayout.removeAllViews();

            mScrollingOffset = 0;
        } else if (mItemsLayout != null) {
            // cache all items
            mItemsRange.set(0, 0);
            mRecycler.recycleItems(mItemsLayout, mFirstItemIdx, mItemsRange);
        }
        invalidate();
    }


    //--------------------------------------------------------------------------
    //
    //  Getters and setters
    //
    //--------------------------------------------------------------------------

    /**
     * Gets count of visible items
     *
     * @return the count of visible items
     */
    public final int getVisibleItems() {
        return mVisibleItems;
    }

    /**
     * Sets the desired count of visible items.
     * Actual amount of visible items depends on spinnerwheel layout parameters.
     * To apply changes and rebuild view call measure().
     *
     * @param count the desired count for visible items
     */
    public void setVisibleItems(final int count) {
        mVisibleItems = count;
    }

    /**
     * Sets all items to have no dim and makes them visible
     *
     * @param isAllVisible
     */
    public void setAllItemsVisible(final boolean isAllVisible) {
        mIsAllVisible = isAllVisible;
        invalidateItemsLayout(false);
    }

    /**
     * Gets view adapter
     *
     * @return the view adapter
     */
    public final AbstractWheelAdapter getViewAdapter() {
        return mViewAdapter;
    }

    /**
     * Sets view adapter. Usually new adapters contain different views, so
     * it needs to rebuild view by calling measure().
     *
     * @param viewAdapter the view adapter
     */
    public void setViewAdapter(final AbstractWheelAdapter viewAdapter) {
        if (mViewAdapter != null) mViewAdapter.unregisterDataSetObserver(mDataObserver);

        mViewAdapter = viewAdapter;
        if (mViewAdapter != null) mViewAdapter.registerDataSetObserver(mDataObserver);

        invalidateItemsLayout(true);

        if (mViewAdapter != null) {
            mViewAdapter.configureInputText(mInputField);
            if (mViewAdapter.isNumeric()) {
                mInputField.setRawInputType(InputType.TYPE_CLASS_NUMBER);
                mInputField.setFilters(new InputFilter[]{
                        new InputTextFilter()
                });
            } else {
                mInputField.setRawInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                mInputField.setFilters(new InputFilter[]{});
            }
        }
    }

    /**
     * Gets current value
     *
     * @return the current value
     */
    public int getCurrentItem() {
        return mCurrentItemIdx;
    }

    /**
     * Sets the current item. Does nothing when index is wrong.
     *
     * @param index    the item index
     * @param animated the animation flag
     */
    public void setCurrentItem(int index, final boolean animated) {
        if (mViewAdapter == null || mViewAdapter.getItemsCount() == 0) {
            return; // throw?
        }

        int itemCount = mViewAdapter.getItemsCount();
        if (index < 0 || index >= itemCount) {
            if (mIsCyclic) {
                while (index < 0) {
                    index += itemCount;
                }
                index %= itemCount;
            } else {
                return; // throw?
            }
        }
        if (index != mCurrentItemIdx) {
            if (animated) {
                int itemsToScroll = index - mCurrentItemIdx;
                if (mIsCyclic) {
                    int scroll = itemCount + Math.min(index, mCurrentItemIdx) - Math.max(index, mCurrentItemIdx);
                    if (scroll < Math.abs(itemsToScroll)) {
                        itemsToScroll = itemsToScroll < 0 ? scroll : -scroll;
                    }
                }
                scroll(itemsToScroll, 0);
            } else {
                mScrollingOffset = 0;
                final int old = mCurrentItemIdx;
                mCurrentItemIdx = index;
                notifyChangeListener(old, mCurrentItemIdx);
                invalidate();
            }
        }
    }

    /**
     * Sets the current item w/o animation. Does nothing when index is wrong.
     *
     * @param index the item index
     */
    public void setCurrentItem(final int index) {
        setCurrentItem(index, false);
    }

    /**
     * Tests if spinnerwheel is cyclic. That means before the 1st item there is shown the last one
     *
     * @return true if spinnerwheel is cyclic
     */
    public final boolean isCyclic() {
        return mIsCyclic;
    }

    /**
     * Set spinnerwheel cyclic flag
     *
     * @param isCyclic the flag to set
     */
    public void setCyclic(final boolean isCyclic) {
        mIsCyclic = isCyclic;
        invalidateItemsLayout(false);
    }

    public void setIsInputEnabled(final boolean isInputEnabled) {
        mIsInputEnabled = isInputEnabled;
    }

    public void setIsResetInputByFocusLose(final boolean isResetInputByFocusLose) {
        mIsResetInputByFocusLose = isResetInputByFocusLose;
    }

    //--------------------------------------------------------------------------
    //
    //  Listener operations
    //
    //--------------------------------------------------------------------------

    public void setOnChangeListener(final OnWheelChangeListener listener) {
        mOnChangeListener = listener;
    }

    protected void notifyChangeListener(final int oldValue, final int newValue) {
        if (mOnChangeListener != null) mOnChangeListener.onWheelChange(this, oldValue, newValue);
    }

    /**
     * Adds spinnerwheel scrolling listener
     *
     * @param listener the listener
     */
    public void addScrollingListener(final OnWheelScrollListener listener) {
        mScrollingListeners.add(listener);
    }

    /**
     * Removes spinnerwheel scrolling listener
     *
     * @param listener the listener
     */
    public void removeScrollingListener(final OnWheelScrollListener listener) {
        mScrollingListeners.remove(listener);
    }

    /**
     * Notifies listeners about starting scrolling
     */
    protected void notifyScrollingListenersAboutStart() {
        for (OnWheelScrollListener listener : mScrollingListeners) {
            listener.onScrollingStarted(this);
        }
    }

    /**
     * Notifies listeners about ending scrolling
     */
    protected void notifyScrollingListenersAboutEnd() {
        for (OnWheelScrollListener listener : mScrollingListeners) {
            listener.onScrollingFinished(this);
        }
    }

    public void setOnClickListener(final OnWheelClickListener listener) {
        mOnClickListener = listener;
    }

    protected void notifyClickListeners(final int item) {
        if (mOnClickListener != null) mOnClickListener.onItemClick(this, item);
    }

    public void setOnInsertListener(final OnWheelInsertListener listener) {
        mOnInsertListener = listener;
    }

    public void notifyInsertListener() {
        if (mOnInsertListener != null) mOnInsertListener.onWheelInsert(this);
    }

    //--------------------------------------------------------------------------
    //
    //  Rebuilding items
    //
    //--------------------------------------------------------------------------

    /**
     * Rebuilds spinnerwheel items if necessary. Caches all unused items.
     *
     * @return true if items are rebuilt
     */
    protected boolean rebuildItems() {
        boolean updated;
        final ItemsRange range = getItemsRange();

        if (mItemsLayout != null) {
            int first = mRecycler.recycleItems(mItemsLayout, mFirstItemIdx, range);
            updated = mFirstItemIdx != first;
            mFirstItemIdx = first;
        } else {
            createItemsLayout();
            updated = true;
        }

        if (!updated) {
            updated = mFirstItemIdx != range.getFirst() || mItemsLayout.getChildCount() != range.getCount();
        }

        if (mFirstItemIdx > range.getFirst() && mFirstItemIdx <= range.getLast()) {
            for (int i = mFirstItemIdx - 1; i >= range.getFirst(); i--) {
                if (!addItemView(i, true)) {
                    break;
                }
                mFirstItemIdx = i;
            }
        } else {
            mFirstItemIdx = range.getFirst();
        }

        int first = mFirstItemIdx;
        for (int i = mItemsLayout.getChildCount(); i < range.getCount(); i++) {
            if (!addItemView(mFirstItemIdx + i, false) && mItemsLayout.getChildCount() == 0) {
                first++;
            }
        }
        mFirstItemIdx = first;

        return updated;
    }

    //----------------------------------
    //  ItemsRange operations
    //----------------------------------

    /**
     * Calculates range for spinnerwheel items
     *
     * @return the items range
     */
    private ItemsRange getItemsRange() {
        if (mIsAllVisible) {
            int baseDimension = getBaseDimension();
            int itemDimension = getItemDimension();
            if (itemDimension != 0) mVisibleItems = baseDimension / itemDimension + 1;
        }

        int start = mCurrentItemIdx - mVisibleItems / 2;
        int end = start + mVisibleItems - (mVisibleItems % 2 == 0 ? 0 : 1);
        if (mScrollingOffset != 0) {
            if (mScrollingOffset > 0) {
                start--;
            } else {
                end++;
            }
        }
        if (!isCyclic()) {
            if (start < 0) start = 0;
            if (mViewAdapter == null) end = 0;
            else if (end > mViewAdapter.getItemsCount()) end = mViewAdapter.getItemsCount();
        }
        mItemsRange.set(start, end - start + 1);
        return mItemsRange;
    }

    /**
     * Checks whether item index is valid
     *
     * @param index the item index
     * @return true if item index is not out of bounds or the spinnerwheel is cyclic
     */
    protected boolean isValidItemIndex(final int index) {
        return (mViewAdapter != null) && (mViewAdapter.getItemsCount() > 0) &&
                (mIsCyclic || (index >= 0 && index < mViewAdapter.getItemsCount()));
    }

    //----------------------------------
    //  Operations with item view
    //----------------------------------

    /**
     * Adds view for item to items layout
     *
     * @param index the item index
     * @param first the flag indicates if view should be first
     * @return true if corresponding item exists and is added
     */
    private boolean addItemView(final int index, final boolean first) {
        final View view = getItemView(index);
        if (view != null) {
            if (first) {
                mItemsLayout.addView(view, 0);
            } else {
                mItemsLayout.addView(view);
            }
            return true;
        }
        return false;
    }

    /**
     * Returns view for specified item
     *
     * @param index the item index
     * @return item view or empty view if index is out of bounds
     */
    private View getItemView(int index) {
        if (mViewAdapter == null || mViewAdapter.getItemsCount() == 0) {
            return null;
        }
        int count = mViewAdapter.getItemsCount();
        if (!isValidItemIndex(index)) {
            return mViewAdapter.getEmptyItem(mRecycler.getEmptyItem(), mItemsLayout);
        } else {
            while (index < 0) {
                index = count + index;
            }
        }
        index %= count;
        return mViewAdapter.getItem(index, mRecycler.getItem(), mItemsLayout);
    }


    //--------------------------------------------------------------------------
    //
    //  Intercepting and processing touch event
    //
    //--------------------------------------------------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled() || getViewAdapter() == null) {
            return true;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                if (getParent() != null) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
                break;

            case MotionEvent.ACTION_UP:
                if (!mIsScrollingPerformed) {
                    int distance = (int) getMotionEventPosition(event) - getBaseDimension() / 2;
                    if (distance > 0) {
                        distance += getItemDimension() / 2;
                    } else {
                        distance -= getItemDimension() / 2;
                    }
                    int items = distance / getItemDimension();
                    if (items != 0 && isValidItemIndex(mCurrentItemIdx + items)) {
                        notifyClickListeners(mCurrentItemIdx + items);
                        setCurrentItem(mCurrentItemIdx + items, false);
                        hideSoftInput(0);
                    }
                    if (items == 0 && isValidItemIndex(mCurrentItemIdx + items)) {
                        showSoftInput();
                    }

                } else {
                    hideSoftInput(0);
                }
                break;
        }
        return mScroller.onTouchEvent(event);
    }

}