/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.spinnerwheel;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Spinner wheel horizontal view.
 *
 * @author Yuri Kanivets
 * @author Dimitri Fedorov
 */
public class WheelHorizontalView extends AbstractWheelView {

    /**
     * The width of the selection divider.
     */
    protected int mSelectionDividerWidth;

    // Item width
    private int mItemWidth = 0;

    //--------------------------------------------------------------------------
    //
    //  Constructors
    //
    //--------------------------------------------------------------------------

    /**
     * Create a new wheel horizontal view.
     *
     * @param context The application environment.
     */
    public WheelHorizontalView(final Context context) {
        this(context, null);
    }

    /**
     * Create a new wheel horizontal view.
     *
     * @param context The application environment.
     * @param attrs   A collection of attributes.
     */
    public WheelHorizontalView(final Context context, final AttributeSet attrs) {
        this(context, attrs, R.attr.abstractWheelViewStyle);
    }

    /**
     * Create a new wheel horizontal view.
     *
     * @param context  the application environment.
     * @param attrs    a collection of attributes.
     * @param defStyle The default style to apply to this view.
     */
    public WheelHorizontalView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }


    //--------------------------------------------------------------------------
    //
    //  Initiating assets and setter for selector paint
    //
    //--------------------------------------------------------------------------

    @Override
    protected void initAttributes(final AttributeSet attrs, final int defStyle) {
        super.initAttributes(attrs, defStyle);

        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.WheelHorizontalView, defStyle, 0);
        mSelectionDividerWidth = a.getDimensionPixelSize(R.styleable.WheelHorizontalView_selectionDividerWidth, DEF_SELECTION_DIVIDER_SIZE);
        a.recycle();
    }

    public void setSelectionDividerWidth(final int selectionDividerWidth) {
        mSelectionDividerWidth = selectionDividerWidth;
    }

    @Override
    public void setSelectorPaintCoeff(final float coeff) {
        if (mItemsDimmedAlpha >= 100) return;

        LinearGradient shader;

        final int w = getMeasuredWidth();
        final int iw = getItemDimension();
        final float p1 = (1 - iw / (float) w) / 2;
        final float p2 = (1 + iw / (float) w) / 2;
        final float z = mItemsDimmedAlpha * (1 - coeff);
        final float c1f = z + 255 * coeff;

        if (mVisibleItems == 2) {
            int c1 = Math.round(c1f) << 24;
            int c2 = Math.round(z) << 24;

            mSelectorShaderColors[0] = c2;
            mSelectorShaderColors[1] = c1;
            mSelectorShaderColors[4] = c1;
            mSelectorShaderColors[5] = c2;
            mSelectorShaderPositions[1] = p1;
            mSelectorShaderPositions[2] = p1;
            mSelectorShaderPositions[3] = p2;
            mSelectorShaderPositions[4] = p2;

            shader = new LinearGradient(0, 0, w, 0, mSelectorShaderColors, mSelectorShaderPositions, Shader.TileMode.CLAMP);
        } else {
            final float p3 = (1 - iw * 3 / (float) w) / 2;
            final float p4 = (1 + iw * 3 / (float) w) / 2;

            final float s = 255 * p3 / p1;
            final float c3f = s * coeff; // here goes some optimized stuff
            final float c2f = z + c3f;

            final int c2 = Math.round(c2f) << 24;

            mSelectorShaderColors[0] = c2;
            mSelectorShaderColors[1] = c2;
            mSelectorShaderColors[2] = c2;
            mSelectorShaderColors[3] = c2;
            mSelectorShaderColors[6] = c2;
            mSelectorShaderColors[7] = c2;
            mSelectorShaderColors[8] = c2;
            mSelectorShaderColors[9] = c2;

            mSelectorShaderPositions[1] = p3;
            mSelectorShaderPositions[2] = p3;
            mSelectorShaderPositions[3] = p1;
            mSelectorShaderPositions[4] = p1;
            mSelectorShaderPositions[5] = p2;
            mSelectorShaderPositions[6] = p2;
            mSelectorShaderPositions[7] = p4;
            mSelectorShaderPositions[8] = p4;
            shader = new LinearGradient(0, 0, w, 0, mSelectorShaderColors, mSelectorShaderPositions, Shader.TileMode.CLAMP);
        }
        mSelectorWheelPaint.setShader(shader);
        invalidate();
    }


    //--------------------------------------------------------------------------
    //
    //  Scroller-specific methods
    //
    //--------------------------------------------------------------------------

    @Override
    protected WheelScroller createScroller(final WheelScroller.ScrollingListener scrollingListener) {
        return new WheelHorizontalScroller(getContext(), scrollingListener);
    }

    @Override
    protected float getMotionEventPosition(final MotionEvent event) {
        return event.getX();
    }


    //--------------------------------------------------------------------------
    //
    //  Base measurements
    //
    //--------------------------------------------------------------------------

    @Override
    protected int getBaseDimension() {
        return getWidth();
    }

    /**
     * Returns height of spinnerwheel item
     *
     * @return the item width
     */
    @Override
    protected int getItemDimension() {
        if (mItemWidth != 0) return mItemWidth;

        if (mItemsLayout != null && mItemsLayout.getChildAt(0) != null) {
            mItemWidth = mItemsLayout.getChildAt(0).getMeasuredWidth();
            return mItemWidth;
        }

        return getBaseDimension() / mVisibleItems;
    }

    //--------------------------------------------------------------------------
    //
    //  Debugging stuff
    //
    //--------------------------------------------------------------------------


    @Override
    protected void onScrollTouchedUp() {
        super.onScrollTouchedUp();
        final int count = mItemsLayout.getChildCount();
        View itm;
        for (int i = 0; i < count; i++) {
            itm = mItemsLayout.getChildAt(i);
            itm.forceLayout(); // forcing layout without re-rendering parent
        }
    }


    //--------------------------------------------------------------------------
    //
    //  Layout creation and measurement operations
    //
    //--------------------------------------------------------------------------

    /**
     * Creates item layouts if necessary
     */
    @Override
    protected void createItemsLayout() {
        if (mItemsLayout == null) {
            mItemsLayout = new LinearLayout(getContext());
            mItemsLayout.setOrientation(LinearLayout.HORIZONTAL);
        }
    }

    @Override
    protected void doItemsLayout() {
        mItemsLayout.layout(0, 0, getMeasuredWidth(), getMeasuredHeight() - 2 * mItemsPadding);
    }

    @Override
    protected void measureLayout() {
        mItemsLayout.setLayoutParams(DEF_PARAMS);
        // XXX: Locating bug
        mItemsLayout.measure(
                MeasureSpec.makeMeasureSpec(getWidth() + getItemDimension(), MeasureSpec.UNSPECIFIED),
                MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.AT_MOST));
    }

    //XXX: Most likely, measurements of mItemsLayout or/and its children are done inconrrectly.
    // Investigate and fix it

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        rebuildItems(); // rebuilding before measuring

        int height = calculateLayoutHeight(heightSize, heightMode);

        int width;
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else {
            width = Math.max(
                    getItemDimension() * (mVisibleItems - mItemOffsetPercent / 100),
                    getSuggestedMinimumWidth());

            if (widthMode == MeasureSpec.AT_MOST) {
                width = Math.min(width, widthSize);
            }
        }
        setMeasuredDimension(width, height);
    }


    /**
     * Calculates control height and creates text layouts
     *
     * @param heightSize the input layout height
     * @param mode       the layout mode
     * @return the calculated control height
     */
    private int calculateLayoutHeight(int heightSize, int mode) {
        mItemsLayout.setLayoutParams(DEF_PARAMS);
        mItemsLayout.measure(
                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.UNSPECIFIED));
        int height = mItemsLayout.getMeasuredHeight();

        if (mode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else {
            height += 2 * mItemsPadding;

            // Check against our minimum width
            height = Math.max(height, getSuggestedMinimumHeight());

            if (mode == MeasureSpec.AT_MOST && heightSize < height) {
                height = heightSize;
            }
        }
        // forcing recalculating
        mItemsLayout.measure(
                // MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                MeasureSpec.makeMeasureSpec(400, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height - 2 * mItemsPadding, MeasureSpec.EXACTLY));

        return height;
    }


    //--------------------------------------------------------------------------
    //
    //  Drawing items
    //
    //--------------------------------------------------------------------------

    @Override
    protected void drawItems(final Canvas canvas) {
        canvas.save();
        final int w = getMeasuredWidth();
        final int h = getMeasuredHeight();
        final int iw = getItemDimension();

        // resetting intermediate bitmap and recreating canvases
        mSpinBitmap.eraseColor(0);
        mSpinCanvas1.setBitmap(mSpinBitmap);
        mSpinCanvas2.setBitmap(mSpinBitmap);

        final int left = (mCurrentItemIdx - mFirstItemIdx) * iw + (iw - getWidth()) / 2;
        mSpinCanvas1.save();
        mSpinCanvas1.translate(-left + mScrollingOffset, mItemsPadding);
        mItemsLayout.draw(mSpinCanvas1);
        mSpinCanvas1.restore();

        mSeparatorsBitmap.eraseColor(0);
        mSeparatorCanvas.setBitmap(mSeparatorsBitmap);

        if (mSelectionDivider != null) {
            // draw the top divider
            final int leftOfLeftDivider = (getWidth() - iw - mSelectionDividerWidth) / 2;
            final int rightOfLeftDivider = leftOfLeftDivider + mSelectionDividerWidth;
            mSeparatorCanvas.save();
            // On Gingerbread setBounds() is ignored resulting in an ugly visual bug.
            mSeparatorCanvas.clipRect(leftOfLeftDivider, 0, rightOfLeftDivider, h);
            mSelectionDivider.setBounds(leftOfLeftDivider, 0, rightOfLeftDivider, h);
            mSelectionDivider.draw(mSeparatorCanvas);
            mSeparatorCanvas.restore();

            mSeparatorCanvas.save();
            // draw the bottom divider
            final int leftOfRightDivider = leftOfLeftDivider + iw;
            final int rightOfRightDivider = rightOfLeftDivider + iw;
            // On Gingerbread setBounds() is ignored resulting in an ugly visual bug.
            mSeparatorCanvas.clipRect(leftOfRightDivider, 0, rightOfRightDivider, h);
            mSelectionDivider.setBounds(leftOfRightDivider, 0, rightOfRightDivider, h);
            mSelectionDivider.draw(mSeparatorCanvas);
            mSeparatorCanvas.restore();
        }

        mSpinCanvas2.drawRect(0, 0, w, h, mSelectorWheelPaint);
        mSeparatorCanvas.drawRect(0, 0, w, h, mSeparatorsPaint);

        canvas.drawBitmap(mSpinBitmap, 0, 0, null);
        canvas.drawBitmap(mSeparatorsBitmap, 0, 0, null);
        canvas.restore();
    }
}