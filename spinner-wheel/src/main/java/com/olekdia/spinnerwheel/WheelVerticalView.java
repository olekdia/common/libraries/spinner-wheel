/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.spinnerwheel;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

/**
 * Spinner wheel vertical view.
 *
 * @author Yuri Kanivets
 * @author Dimitri Fedorov
 */
public class WheelVerticalView extends AbstractWheelView {

    /**
     * The height of the selection divider.
     */
    protected int mSelectionDividerHeight;

    // Cached item height
    private int mItemHeight = 0;

    //--------------------------------------------------------------------------
    //
    //  Constructors
    //
    //--------------------------------------------------------------------------

    /**
     * Create a new wheel vertical view.
     *
     * @param context The application environment.
     */
    public WheelVerticalView(Context context) {
        this(context, null);
    }

    /**
     * Create a new wheel vertical view.
     *
     * @param context The application environment.
     * @param attrs   A collection of attributes.
     */
    public WheelVerticalView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.abstractWheelViewStyle);
    }

    /**
     * Create a new wheel vertical view.
     *
     * @param context  the application environment.
     * @param attrs    a collection of attributes.
     * @param defStyle The default style to apply to this view.
     */
    public WheelVerticalView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    //--------------------------------------------------------------------------
    //
    //  Initiating assets and setter for selector paint
    //
    //--------------------------------------------------------------------------

    @Override
    protected void initAttributes(AttributeSet attrs, int defStyle) {
        super.initAttributes(attrs, defStyle);

        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.WheelVerticalView, defStyle, 0);
        mSelectionDividerHeight = a.getDimensionPixelSize(R.styleable.WheelVerticalView_selectionDividerHeight, DEF_SELECTION_DIVIDER_SIZE);
        a.recycle();
    }

    @Override
    public void setSelectorPaintCoeff(final float coeff) {
        LinearGradient shader;

        final int h = getMeasuredHeight();
        final int ih = getItemDimension();
        final float p1 = (1 - ih / (float) h) / 2;
        final float p2 = (1 + ih / (float) h) / 2;
        final float z = mItemsDimmedAlpha * (1 - coeff);
        final float c1f = z + 255 * coeff;

        if (mVisibleItems == 2) {
            final int c1 = Math.round(c1f) << 24;
            final int c2 = Math.round(z) << 24;

            mSelectorShaderColors[0] = c2;
            mSelectorShaderColors[1] = c1;
            mSelectorShaderColors[4] = c1;
            mSelectorShaderColors[5] = c2;
            mSelectorShaderPositions[1] = p1;
            mSelectorShaderPositions[2] = p1;
            mSelectorShaderPositions[3] = p2;
            mSelectorShaderPositions[4] = p2;
            shader = new LinearGradient(0, 0, 0, h, mSelectorShaderColors, mSelectorShaderPositions, Shader.TileMode.CLAMP);
        } else {
            final float p3 = (1 - ih * 3 / (float) h) / 2;
            final float p4 = (1 + ih * 3 / (float) h) / 2;

            final float s = 255 * p3 / p1;
            final float c3f = s * coeff; // here goes some optimized stuff
            final float c2f = z + c3f;

            final int c1 = Math.round(c1f) << 24;
            final int c2 = Math.round(c2f) << 24;
            final int c3 = Math.round(c3f) << 24;

            mSelectorShaderColors[1] = c3;
            mSelectorShaderColors[2] = c2;
            mSelectorShaderColors[3] = c1;
            mSelectorShaderColors[6] = c1;
            mSelectorShaderColors[7] = c2;
            mSelectorShaderColors[8] = c3;

            mSelectorShaderPositions[1] = p3;
            mSelectorShaderPositions[2] = p3;
            mSelectorShaderPositions[3] = p1;
            mSelectorShaderPositions[4] = p1;
            mSelectorShaderPositions[5] = p2;
            mSelectorShaderPositions[6] = p2;
            mSelectorShaderPositions[7] = p4;
            mSelectorShaderPositions[8] = p4;
            shader = new LinearGradient(0, 0, 0, h, mSelectorShaderColors, mSelectorShaderPositions, Shader.TileMode.CLAMP);
        }
        mSelectorWheelPaint.setShader(shader);
        invalidate();
    }

    //--------------------------------------------------------------------------
    //
    //  Scroller-specific methods
    //
    //--------------------------------------------------------------------------

    @Override
    protected WheelScroller createScroller(WheelScroller.ScrollingListener scrollingListener) {
        return new WheelVerticalScroller(getContext(), scrollingListener);
    }

    @Override
    protected float getMotionEventPosition(MotionEvent event) {
        return event.getY();
    }

    //--------------------------------------------------------------------------
    //
    //  Base measurements
    //
    //--------------------------------------------------------------------------

    @Override
    protected int getBaseDimension() {
        return getHeight();
    }

    /**
     * Returns height of the spinnerwheel
     *
     * @return the item height
     */
    @Override
    protected int getItemDimension() {
        if (mItemHeight != 0) {
            return mItemHeight;
        }

        if (mItemsLayout != null && mItemsLayout.getChildAt(0) != null) {
            mItemHeight = mItemsLayout.getChildAt(0).getMeasuredHeight();
            return mItemHeight;
        }

        return getBaseDimension() / mVisibleItems;
    }

    //--------------------------------------------------------------------------
    //
    //  Layout creation and measurement operations
    //
    //--------------------------------------------------------------------------

    /**
     * Creates item layout if necessary
     */
    @Override
    protected void createItemsLayout() {
        if (mItemsLayout == null) {
            mItemsLayout = new LinearLayout(getContext());
            mItemsLayout.setOrientation(LinearLayout.VERTICAL);
        }
    }

    @Override
    protected void doItemsLayout() {
        mItemsLayout.layout(0, 0, getMeasuredWidth() - 2 * mItemsPadding, getMeasuredHeight());
    }


    @Override
    protected void measureLayout() {
        mItemsLayout.setLayoutParams(DEF_PARAMS);

        mItemsLayout.measure(
                MeasureSpec.makeMeasureSpec(getWidth() - 2 * mItemsPadding, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
        );

    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        final int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        final int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        rebuildItems(); // rebuilding before measuring

        int width = calculateLayoutWidth(widthSize, widthMode);

        int height;
        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else {
            height = Math.max(
                    getItemDimension() * (mVisibleItems - mItemOffsetPercent / 100) + getPaddingTop() + getPaddingBottom(),
                    getSuggestedMinimumHeight());

            if (heightMode == MeasureSpec.AT_MOST) {
                height = Math.min(height, heightSize);
            }
        }
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        final int msrdWdth = r - l;
        final int msrdHght = b - t;

        // Input text centered horizontally.
        final int inptTxtMsrdWdth = mInputField.getMeasuredWidth();
        final int inptTxtMsrdHght = mInputField.getMeasuredHeight();
        final int inptTxtLeft = (msrdWdth - inptTxtMsrdWdth) / 2;
        final int inptTxtTop = (msrdHght - inptTxtMsrdHght) / 2;
        final int inptTxtRight = inptTxtLeft + inptTxtMsrdWdth;
        final int inptTxtBottom = inptTxtTop + inptTxtMsrdHght;
        mInputField.layout(inptTxtLeft, inptTxtTop, inptTxtRight, inptTxtBottom);
    }

    /**
     * Calculates control width
     *
     * @param widthSize the input layout width
     * @param mode      the layout mode
     * @return the calculated control width
     */
    private int calculateLayoutWidth(int widthSize, int mode) {
        mItemsLayout.setLayoutParams(DEF_PARAMS);
        mItemsLayout.measure(
                MeasureSpec.makeMeasureSpec(widthSize, MeasureSpec.UNSPECIFIED),
                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
        );
        int width = mItemsLayout.getMeasuredWidth();

        if (mode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else {
            width += 2 * mItemsPadding;

            // Check against our minimum width
            width = Math.max(width, getSuggestedMinimumWidth());

            if (mode == MeasureSpec.AT_MOST && widthSize < width) {
                width = widthSize;
            }
        }

        // forcing recalculating
        mItemsLayout.measure(
                MeasureSpec.makeMeasureSpec(width - 2 * mItemsPadding, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
        );

        return width;
    }


    //--------------------------------------------------------------------------
    //
    //  Drawing items
    //
    //--------------------------------------------------------------------------

    @Override
    protected void drawItems(final Canvas canvas) {
        canvas.save();
        final int w = getMeasuredWidth();
        final int h = getMeasuredHeight();
        final int ih = getItemDimension();

        // resetting intermediate bitmap and recreating canvases
        mSpinBitmap.eraseColor(0);
        mSpinCanvas1.setBitmap(mSpinBitmap);
        mSpinCanvas2.setBitmap(mSpinBitmap);

        final int top = (mCurrentItemIdx - mFirstItemIdx) * ih + (ih - getHeight()) / 2;
        mSpinCanvas1.save();
        mSpinCanvas1.translate(mItemsPadding, -top + mScrollingOffset);
        mItemsLayout.draw(mSpinCanvas1);
        mSpinCanvas1.restore();

        mSeparatorsBitmap.eraseColor(0);
        mSeparatorCanvas.setBitmap(mSeparatorsBitmap);

        if (mSelectionDivider != null) {
            // draw the top divider
            final int topOfTopDivider = (getHeight() - ih - mSelectionDividerHeight) / 2;
            final int bottomOfTopDivider = topOfTopDivider + mSelectionDividerHeight;
            mSelectionDivider.setBounds(0, topOfTopDivider, w, bottomOfTopDivider);
            mSelectionDivider.draw(mSeparatorCanvas);

            // draw the bottom divider
            final int topOfBottomDivider = topOfTopDivider + ih;
            final int bottomOfBottomDivider = bottomOfTopDivider + ih;
            mSelectionDivider.setBounds(0, topOfBottomDivider, w, bottomOfBottomDivider);
            mSelectionDivider.draw(mSeparatorCanvas);
        }

        mSpinCanvas2.drawRect(0, 0, w, h, mSelectorWheelPaint);
        mSeparatorCanvas.drawRect(0, 0, w, h, mSeparatorsPaint);

        canvas.drawBitmap(mSpinBitmap, 0, 0, null);
        canvas.drawBitmap(mSeparatorsBitmap, 0, 0, null);
        canvas.restore();

        if (mLabel != null && !mLabel.equals("")) {
            canvas.drawText(mLabel, mLabelX, h - mLabelOffset, mLabelPaint);
        }
    }
}