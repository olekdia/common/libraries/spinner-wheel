/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.spinnerwheel.adapters;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.olekdia.spinnerwheel.AbstractWheel;

import java.util.LinkedList;
import java.util.List;

/**
 * Abstract Wheel adapter.
 */
public abstract class AbstractWheelAdapter {

    // Observers
    private List<DataSetObserver> mDatasetObservers;

    /**
     * Gets items count
     * @return the count of spinnerwheel items
     */
    public abstract int getItemsCount();

    /**
     * Get a View that displays the data at the specified position in the data set
     *
     * @param index the item index
     * @param convertView the old view to reuse if possible
     * @param parent the parent that this view will eventually be attached to
     * @return the spinnerwheel item View
     */
    public abstract View getItem(int index, View convertView, ViewGroup parent);

    /**
     * Get a View that displays an empty spinnerwheel item placed before the first or after
     * the last spinnerwheel item.
     *
     * @param convertView the old view to reuse if possible
     * @param parent the parent that this view will eventually be attached to
     * @return the empty item View
     */
    public View getEmptyItem(View convertView, ViewGroup parent) {
        return null;
    }

    /**
     * Register an observer that is called when changes happen to the data used by this adapter.
     * @param observer the observer to be registered
     */
    public void registerDataSetObserver(final DataSetObserver observer) {
        if (mDatasetObservers == null) {
            mDatasetObservers = new LinkedList<DataSetObserver>();
        }
        mDatasetObservers.add(observer);
    }

    /**
     * Unregister an observer that has previously been registered
     * @param observer the observer to be unregistered
     */
    public void unregisterDataSetObserver(final DataSetObserver observer) {
        if (mDatasetObservers != null) {
            mDatasetObservers.remove(observer);
        }
    }

    /**
     * Notifies observers about data changing
     */
    protected void notifyDataChangedEvent() {
        if (mDatasetObservers != null) {
            for (DataSetObserver observer : mDatasetObservers) {
                observer.onChanged();
            }
        }
    }

    /**
     * Notifies observers about invalidating data
     */
    protected void notifyDataInvalidatedEvent() {
        if (mDatasetObservers != null) {
            for (DataSetObserver observer : mDatasetObservers) {
                observer.onInvalidated();
            }
        }
    }

    public abstract CharSequence getItemText(int index);

    public abstract int getItemValue(int index);

    public abstract int getMinValue();

    public abstract int getMaxValue();

    public abstract boolean isNumeric();

    public abstract void configureInputText(TextView view);

    public abstract int findPosInputValue(AbstractWheel wheel);

    public abstract boolean isLastItem(int itemIndex);
}