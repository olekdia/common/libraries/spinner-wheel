/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.olekdia.spinnerwheel.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Abstract spinnerwheel adapter provides common functionality for adapters.
 */
public abstract class AbstractWheelTextAdapter extends AbstractWheelAdapter {

    /** Text view resource. Used as a default view for adapter. */
    public static final int TEXT_VIEW_ITEM_RESOURCE = -1;

    /** No resource constant. */
    protected static final int NO_RESOURCE = 0;

    /** Default text color */
    public static final int DEFAULT_TEXT_COLOR = 0xFF101010;

    /** Default text color */
    public static final int LABEL_COLOR = 0xFF700070;

    /** Default text size */
    public static final int DEFAULT_TEXT_SIZE = 24;

    /** Default text padding */
    public static final int DEFAULT_TEXT_VERT_PADDING = 20;

    /// Custom text typeface
    private Typeface mTextTypeface;

    // Text settings
    private int mTextColor = DEFAULT_TEXT_COLOR;
    private int mTextSize = DEFAULT_TEXT_SIZE;
    private int mTextVertPadding = DEFAULT_TEXT_VERT_PADDING;

    // Current mContext
    protected Context mContext;
    // Layout mInflater
    protected LayoutInflater mInflater;

    // Items resources
    protected int mItemResourceId;
    protected int mItemTextResourceId;

    // Empty items resources
    protected int mEmptyItemResourceId;

    /**
     * Constructor
     * @param context the current mContext
     */
    protected AbstractWheelTextAdapter(final Context context) {
        this(context, TEXT_VIEW_ITEM_RESOURCE);
    }

    /**
     * Constructor
     * @param context the current mContext
     * @param itemResource the resource ID for a layout file containing a TextView to use when instantiating items views
     */
    protected AbstractWheelTextAdapter(final Context context, final int itemResource) {
        this(context, itemResource, NO_RESOURCE);
    }

    /**
     * Constructor
     * @param context the current mContext
     * @param itemResource the resource ID for a layout file containing a TextView to use when instantiating items views
     * @param itemTextResource the resource ID for a text view in the item layout
     */
    protected AbstractWheelTextAdapter(final Context context, final int itemResource, final int itemTextResource) {
        mContext = context;
        mItemResourceId = itemResource;
        mItemTextResourceId = itemTextResource;

        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Gets text color
     * @return the text color
     */
    public final int getTextColor() {
        return mTextColor;
    }

    /**
     * Sets text color
     * @param textColor the text color to set
     */
    public final void setTextColor(final int textColor) {
        mTextColor = textColor;
    }

    public final int getTextVertPadding() {
        return mTextVertPadding;
    }

    public final void setTextVertPadding(final int textVertPadding) {
        mTextVertPadding = textVertPadding;
    }

    /**
     * Sets text typeface
     * @param typeface typeface to set
     */
    public final void setTextTypeface(final Typeface typeface) {
        mTextTypeface = typeface;
    }

    /**
     * Gets text size
     * @return the text size
     */
    public final int getTextSize() {
        return mTextSize;
    }

    /**
     * Sets text size
     * @param textSize the text size to set
     */
    public final void setTextSize(final int textSize) {
        mTextSize = textSize;
    }

    /**
     * Gets resource Id for items views
     * @return the item resource Id
     */
    public final int getItemResource() {
        return mItemResourceId;
    }

    /**
     * Sets resource Id for items views
     * @param itemResourceId the resource Id to set
     */
    public final void setItemResource(final int itemResourceId) {
        mItemResourceId = itemResourceId;
    }

    /**
     * Gets resource Id for text view in item layout
     * @return the item text resource Id
     */
    public final int getItemTextResource() {
        return mItemTextResourceId;
    }

    /**
     * Sets resource Id for text view in item layout
     * @param itemTextResourceId the item text resource Id to set
     */
    public final void setItemTextResource(final int itemTextResourceId) {
        mItemTextResourceId = itemTextResourceId;
    }

    /**
     * Gets resource Id for empty items views
     * @return the empty item resource Id
     */
    public final int getEmptyItemResource() {
        return mEmptyItemResourceId;
    }

    /**
     * Sets resource Id for empty items views
     * @param emptyItemResourceId the empty item resource Id to set
     */
    public final void setEmptyItemResource(final int emptyItemResourceId) {
        mEmptyItemResourceId = emptyItemResourceId;
    }

    @Override
    public View getItem(final int index, View convertView, final ViewGroup parent) {
        if (index >= 0 && index < getItemsCount()) {
            if (convertView == null) convertView = getView(mItemResourceId, parent);

            final TextView textView = getTextView(convertView, mItemTextResourceId);
            if (textView != null) {
                CharSequence text = getItemText(index);
                if (text == null) text = "";

                textView.setText(text);
                configureTextView(textView);
            }
            return convertView;
        }
        return null;
    }

    @Override
    public View getEmptyItem(View convertView, final ViewGroup parent) {
        if (convertView == null) convertView = getView(mEmptyItemResourceId, parent);

        if (convertView instanceof TextView) configureTextView((TextView) convertView);

        return convertView;
    }

    /**
     * Configures text view. Is called for the TEXT_VIEW_ITEM_RESOURCE views.
     * @param view the text view to be configured
     */
    protected void configureTextView(final TextView view) {
        if (mItemResourceId == TEXT_VIEW_ITEM_RESOURCE) {
            view.setTextColor(mTextColor);
            view.setGravity(Gravity.CENTER);
            view.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
            view.setLines(1);
            view.setPadding(0, mTextVertPadding, 0, mTextVertPadding);
        }

        if (mTextTypeface != null) {
            view.setTypeface(mTextTypeface);
        } else {
            view.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL);
        }
    }

    /**
     * Loads a text view from view
     * @param view the text view or layout containing it
     * @param textResource the text resource Id in layout
     * @return the loaded text view
     */
    private TextView getTextView(final View view, final int textResource) {
        TextView text = null;
        try {
            if (textResource == NO_RESOURCE && view instanceof TextView) {
                text = (TextView) view;
            } else if (textResource != NO_RESOURCE) {
                text = view.findViewById(textResource);
            }
        } catch (ClassCastException e) {
            Log.e("AbstractWheelAdapter", "You must supply a resource ID for a TextView");
            throw new IllegalStateException(
                    "AbstractWheelAdapter requires the resource ID to be a TextView", e);
        }

        return text;
    }

    /**
     * Loads view from resources
     * @param resource the resource Id
     * @return the loaded view or null if resource is not set
     */
    private View getView(final int resource, final ViewGroup parent) {
        switch (resource) {
            case NO_RESOURCE:
                return null;
            case TEXT_VIEW_ITEM_RESOURCE:
                return new TextView(mContext);
            default:
                return mInflater.inflate(resource, parent, false);
        }
    }

    public void configureInputText(final TextView view) {
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
        view.setTextColor(mTextColor);
    }
}