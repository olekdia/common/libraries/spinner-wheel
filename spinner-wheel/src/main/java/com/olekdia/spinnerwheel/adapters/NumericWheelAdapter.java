/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.spinnerwheel.adapters;

import android.content.Context;

import com.olekdia.spinnerwheel.AbstractWheel;
import com.olekdia.commonhelpers.LocaleHelper;

/**
 * Numeric Wheel adapter.
 */
public class NumericWheelAdapter extends AbstractWheelTextAdapter {

    /** The default min value */
    public static final int DEFAULT_MAX_VALUE = 9;

    /** The default max value */
    private static final int DEFAULT_MIN_VALUE = 0;

    // Values
    protected int mMinValue;
    protected int mMaxValue;

    protected final int mNumeralSystem;

    /**
     * Constructor
     * @param context the current mContext
     */
    public NumericWheelAdapter(final Context context) {
        this(context, DEFAULT_MIN_VALUE, DEFAULT_MAX_VALUE);
    }

    /**
     * Constructor
     * @param context the current mContext
     * @param minValue the spinnerwheel min value
     * @param maxValue the spinnerwheel max value
     */
    public NumericWheelAdapter(final Context context, final int minValue, final int maxValue) {
        this(context, minValue, maxValue, LocaleHelper.WESTERN_ARABIC);
    }

    /**
     * Constructor
     * @param context the current mContext
     * @param minValue the spinnerwheel min value
     * @param maxValue the spinnerwheel max value
     * @param numeralSystem
     */
    public NumericWheelAdapter(final Context context, final int minValue, final int maxValue, final int numeralSystem) {
        super(context);

        mMinValue = minValue;
        mMaxValue = maxValue;
        mNumeralSystem = numeralSystem;
    }

    public final void setMinValue(final int minValue) {
        mMinValue = minValue;
        notifyDataInvalidatedEvent();
    }

    public final void setMaxValue(final int maxValue) {
        mMaxValue = maxValue;
        notifyDataInvalidatedEvent();
    }

    @Override
    public CharSequence getItemText(final int index) {
        if (index >= 0 && index < getItemsCount()) {
            final int value = mMinValue + index;
            return LocaleHelper.formatInt(value, mNumeralSystem);
        } else {
            return null;
        }
    }

    @Override
    public int getItemValue(final int index) {
        if (index >= 0 && index < getItemsCount()) {
            return mMinValue + index;
        } else {
            return -1;
        }
    }

    @Override
    public int getItemsCount() {
        return mMaxValue - mMinValue + 1;
    }

    @Override
    public final boolean isNumeric() {
        return true;
    }

    public final int getMinValue() {
        return mMinValue;
    }

    public final int getMaxValue() {
        return mMaxValue;
    }

    @Override
    public int findPosInputValue(final AbstractWheel wheel) {
        final String str = wheel.getInputField().getText().toString();
        try {
            final int current = Integer.parseInt(str);
            return current - mMinValue;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean isLastItem(final int itemIndex) {
        return (itemIndex + mMinValue) == mMaxValue;
    }
}