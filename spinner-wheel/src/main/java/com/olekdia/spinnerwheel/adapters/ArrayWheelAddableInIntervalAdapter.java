/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.olekdia.spinnerwheel.adapters;

import android.content.Context;

import com.olekdia.commonhelpers.LocaleHelper;
import com.olekdia.spinnerwheel.AbstractWheel;

import java.util.ArrayList;

/**
 * The simple Array spinnerwheel adapter
 */
public class ArrayWheelAddableInIntervalAdapter extends AbstractWheelTextAdapter {

    protected final ArrayList<Integer> mItems;

    protected int mMinValue;
    protected int mMaxValue;

    protected final int mNumeralSystem;

    /**
     * Constructor
     *
     * @param context the current mContext
     * @param items   the mItems
     */
    public ArrayWheelAddableInIntervalAdapter(final Context context, final ArrayList<Integer> items,
                                              final int minValue, final int maxValue) {
        this(context, items, minValue, maxValue, LocaleHelper.WESTERN_ARABIC);
    }

    public ArrayWheelAddableInIntervalAdapter(final Context context, final ArrayList<Integer> items,
                                              final int minValue, final int maxValue, final int numeralSystem) {
        super(context);

        mItems = items == null ? new ArrayList<Integer>() : items;

        mMinValue = minValue;
        mMaxValue = maxValue;

        mNumeralSystem = numeralSystem;
    }

    @Override
    public CharSequence getItemText(final int index) {
        return index >= 0 && index < mItems.size() ? LocaleHelper.formatInt(mItems.get(index), mNumeralSystem) : null;
    }

    @Override
    public int getItemValue(int index) {
        return index >= 0 && index < mItems.size() ? mItems.get(index) : -1;
    }

    @Override
    public final int getItemsCount() {
        return mItems.size();
    }

    @Override
    public final boolean isNumeric() {
        return true;
    }

    @Override
    public final int getMinValue() {
        return mMinValue;
    }

    @Override
    public final int getMaxValue() {
        return mMaxValue;
    }

    @Override
    public int findPosInputValue(final AbstractWheel wheel) {
        final String str = wheel.getInputField().getText().toString();
        if (str.isEmpty()) {
            return 0;
        }
        int value = Integer.parseInt(str);
        if (value > 0 && value < mMinValue) {
            return 0;
        }
        for (int i = 0; i < mItems.size(); i++) {
            int result = mItems.get(i).compareTo(value);
            if (result == 0) return i;

            if (result > 0) {
                mItems.add(i, value);
                wheel.notifyInsertListener();
                return i;
            }
            if (result < 0 && i == (mItems.size() - 1)) {
                mItems.add(value);
                wheel.notifyInsertListener();
                return getItemsCount() - 1;
            }
        }
        return -1;
    }

    @Override
    public boolean isLastItem(final int currentItemIdx) {
        return currentItemIdx == (mItems.size() - 1);
    }

    public final ArrayList<Integer> getItems() {
        return mItems;
    }

    public void addValue(final int value) {
        if (value > 0 && value < mMinValue) {
            return;
        }
        for (int j = 0; j < mItems.size(); j++) {
            int result = mItems.get(j).compareTo(value);
            if (result == 0) {
                break;
            }
            if (result > 0) {
                mItems.add(j, value);
                break;
            }
            if (result < 0 && j == (mItems.size() - 1)) {
                mItems.add(value);
                break;
            }
        }
    }
}