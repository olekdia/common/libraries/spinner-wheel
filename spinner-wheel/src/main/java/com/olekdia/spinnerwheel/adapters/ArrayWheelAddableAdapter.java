/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.olekdia.spinnerwheel.adapters;

import android.content.Context;

import com.olekdia.spinnerwheel.AbstractWheel;

import java.util.ArrayList;

/**
 * The simple Array spinnerwheel adapter
 */
public class ArrayWheelAddableAdapter extends AbstractWheelTextAdapter {

    protected ArrayList<String> mItems;
    protected boolean mIsIgnoreCase;

    /**
     * Constructor
     *
     * @param context the current mContext
     * @param items   the mItems
     */
    public ArrayWheelAddableAdapter(final Context context, final ArrayList<String> items, final boolean isIgnoreCase) {
        super(context);

        mItems = items == null ? new ArrayList<String>() : items;
        mIsIgnoreCase = isIgnoreCase;
    }

    @Override
    public CharSequence getItemText(final int index) {
        return index >= 0 && index < mItems.size() ? mItems.get(index) : null;
    }

    @Override
    public final int getItemValue(final int index) {
        return -1;
    }

    @Override
    public int getItemsCount() {
        return mItems.size();
    }

    @Override
    public final boolean isNumeric() {
        return false;
    }

    @Override
    public final int getMinValue() {
        return -1;
    }

    @Override
    public final int getMaxValue() {
        return -1;
    }

    @Override
    public int findPosInputValue(final AbstractWheel wheel) {
        final String str = wheel.getInputField().getText().toString();
        for (int i = 0; i < mItems.size(); i++) {
            if (mIsIgnoreCase ? mItems.get(i).equalsIgnoreCase(str) : mItems.get(i).equals(str)) {
                return i;
            }
            if (i == (mItems.size() - 1) && !str.isEmpty()) {
                mItems.add(str);
                wheel.notifyInsertListener();
                return getItemsCount() - 1;
            }
        }
        return -1;
    }

    @Override
    public final boolean isLastItem(final int currentItemIdx) {
        return currentItemIdx == (mItems.size() - 1);
    }
}