/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.olekdia.spinnerwheel.adapters;

import android.content.Context;

import com.olekdia.spinnerwheel.AbstractWheel;

/**
 * The simple Array spinnerwheel adapter
 */
public class ArrayWheelAdapter extends AbstractWheelTextAdapter {

    // mItems
    protected final CharSequence mItems[];

    /**
     * Constructor
     * @param context the current mContext
     * @param items the mItems
     */
    public ArrayWheelAdapter(final Context context, final CharSequence items[]) {
        super(context);

        //setEmptyItemResource(TEXT_VIEW_ITEM_RESOURCE);
        mItems = items;
    }

    @Override
    public CharSequence getItemText(final int index) {
        return index >= 0 && index < mItems.length ? mItems[index] : null;
    }

    @Override
    public final int getItemValue(int index) {
        return -1;
    }

    @Override
    public final int getItemsCount() {
        return mItems.length;
    }

    @Override
    public final boolean isNumeric() {
        return false;
    }

    @Override
    public final int getMinValue() {
        return -1;
    }

    @Override
    public final int getMaxValue() {
        return -1;
    }

    @Override
    public int findPosInputValue(final AbstractWheel wheel) {
        final String str = wheel.getInputField().getText().toString();
        for (int i = 0; i < mItems.length; i++) {
            if (mItems[i].equals(str)) return i;
        }
        return -1;
    }

    @Override
    public final boolean isLastItem(final int itemIndex) {
        return itemIndex == (mItems.length - 1);
    }
}