/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.spinnerwheel;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;

/**
 * Abstract spinner spinnerwheel view.
 * This class should be subclassed.
 *
 * @author Yuri Kanivets
 * @author Dimitri Fedorov
 */
public abstract class AbstractWheelView extends AbstractWheel {

    //----------------------------------
    //  Default properties values
    //----------------------------------

    protected static final int DEF_ITEMS_DIMMED_ALPHA = 50; // 60 in ICS

    protected static final int DEF_SELECTION_DIVIDER_ACTIVE_ALPHA = 255;

    protected static final int DEF_ITEM_OFFSET_PERCENT = 10;

    protected static final int DEF_ITEM_PADDING = 10;

    protected static final int DEF_SELECTION_DIVIDER_SIZE = 2;

    protected static final int DEF_LABEL_COLOR = 0xFF000000;

    protected static final int DEF_LABEL_FONT_SIZE = 10;

    protected static final ViewGroup.LayoutParams DEF_PARAMS = new ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    //----------------------------------
    //  Class properties
    //----------------------------------

    // configurable properties

    /** The alpha of the selector spinnerwheel when it is dimmed. */
    protected int mItemsDimmedAlpha;

    /** Top and bottom items offset */
    protected int mItemOffsetPercent;

    /** Left and right padding value */
    protected int mItemsPadding;

    /** Divider for showing item to be selected while scrolling */
    protected Drawable mSelectionDivider;

    protected String mLabel;

    protected int mLabelColor;

    protected int mLabelFontSize;

    protected float mLabelOffset;

    protected float mLabelX;

    // the rest
    /**
     * The {@link Paint} for drawing the label.
     */
    protected Paint mLabelPaint;

    /**
     * The {@link Paint} for drawing the selector.
     */
    protected Paint mSelectorWheelPaint;

    /**
     * The {@link Paint} for drawing the separators.
     */
    protected Paint mSeparatorsPaint;

    /**
     * for dimming the selector spinnerwheel.
     */
    protected Animator mDimSelectorWheelAnimator;

    /**
     * The property for setting the selector paint.
     */
    protected static final String PROPERTY_SELECTOR_PAINT_COEFF = "selectorPaintCoeff";

    protected Bitmap mSpinBitmap;
    protected Bitmap mSeparatorsBitmap;

    protected Canvas mSpinCanvas1;
    protected Canvas mSpinCanvas2;
    protected Canvas mSeparatorCanvas;


    //--------------------------------------------------------------------------
    //
    //  Constructor
    //
    //--------------------------------------------------------------------------

    public AbstractWheelView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    //--------------------------------------------------------------------------
    //
    //  Initiating assets and setters for paints
    //
    //--------------------------------------------------------------------------

    @Override
    protected void initAttributes(final AttributeSet attrs, final int defStyle) {
        super.initAttributes(attrs, defStyle);

        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.AbstractWheelView, defStyle, 0);
        mItemsDimmedAlpha = a.getInt(R.styleable.AbstractWheelView_itemsDimmedAlpha, DEF_ITEMS_DIMMED_ALPHA);
        mItemOffsetPercent = a.getInt(R.styleable.AbstractWheelView_itemOffsetPercent, DEF_ITEM_OFFSET_PERCENT);
        mItemsPadding = a.getDimensionPixelSize(R.styleable.AbstractWheelView_itemsPadding, DEF_ITEM_PADDING);
        mSelectionDivider = a.getDrawable(R.styleable.AbstractWheelView_selectionDivider);
        mLabelColor = a.getColor(R.styleable.AbstractWheelView_labelColor, DEF_LABEL_COLOR);
        mLabelFontSize = a.getDimensionPixelSize(R.styleable.AbstractWheelView_labelFontSize, DEF_LABEL_FONT_SIZE);
        mLabel = a.getString(R.styleable.AbstractWheelView_labelText);

        a.recycle();
    }

    @Override
    protected void initData(final Context context) {
        super.initData(context);

        // creating animators
        mDimSelectorWheelAnimator = ObjectAnimator.ofFloat(this, PROPERTY_SELECTOR_PAINT_COEFF, 1, 0);

        // creating paints
        mSeparatorsPaint = new Paint();
        mSeparatorsPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        mSeparatorsPaint.setAlpha(DEF_SELECTION_DIVIDER_ACTIVE_ALPHA);

        mSelectorWheelPaint = new Paint();
        mSelectorWheelPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

        mLabelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLabelPaint.setTextAlign(Paint.Align.CENTER);
        mLabelPaint.setStyle(Paint.Style.STROKE);
        mLabelPaint.setColor(mLabelColor);
        mLabelPaint.setTextSize(mLabelFontSize);
        mLabelOffset = mLabelPaint.getFontMetrics().descent;

        mSpinCanvas1 = new Canvas();
        mSpinCanvas2 = new Canvas();
        mSeparatorCanvas = new Canvas();
    }

    /**
     * Recreates assets (like bitmaps) when layout size has been changed
     *
     * @param width New spinnerwheel width
     * @param height New spinnerwheel height
     */
    @Override
    protected void recreateAssets(int width, int height) {
        mSpinBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);
        mSeparatorsBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);
        setSelectorPaintCoeff(0);

        invalidateLabel();
    }

    private void invalidateLabel() {
        final int w = getMeasuredWidth();
        if (w > 0 && mLabel != null && mLabel.length() > 2) {
            float textW = mLabelPaint.measureText(mLabel);
            if (w < textW) {
                mLabelPaint.setTextAlign(Paint.Align.LEFT);
                mLabelX = 0;
                final StringBuilder b = new StringBuilder(12);

                String tmpLabel = mLabel;
                final int length = mLabel.length();
                int i = 2;
                while (w < textW && i < length) {
                    tmpLabel = b.append(mLabel.substring(0, length - i)).append("..").toString();
                    textW = mLabelPaint.measureText(tmpLabel);
                    i++;
                    b.setLength(0);
                }
                mLabel = tmpLabel;
            } else {
                mLabelPaint.setTextAlign(Paint.Align.CENTER);
                mLabelX = w / 2;
            }
        }
    }

    public final void setLabel(final String label) {
        mLabel = label;
        invalidateLabel();
        invalidate();
    }

    /**
     * Sets the <code>coeff</code> of the {@link Paint} for drawing
     * the selector spinnerwheel.
     *
     * @param coeff Coefficient from 0 (selector is passive) to 1 (selector is active)
     */
    public abstract void setSelectorPaintCoeff(float coeff);

    public void setSelectionDivider(final Drawable selectionDivider) {
        this.mSelectionDivider = selectionDivider;
    }

    //--------------------------------------------------------------------------
    //
    //  Processing mScroller events
    //
    //--------------------------------------------------------------------------

    @Override
    protected void onScrollTouched() {
        mDimSelectorWheelAnimator.cancel();
        setSelectorPaintCoeff(1);
    }

    @Override
    protected void onScrollTouchedUp() {
        super.onScrollTouchedUp();
        fadeSelectorWheel(750);
    }

    @Override
    protected void onScrollFinished() {
        fadeSelectorWheel(500);
    }

    //----------------------------------
    //  Animating components
    //----------------------------------

    /**
     * Fade the selector spinnerwheel via an animation.
     *
     * @param animationDuration The duration of the animation.
     */
    private void fadeSelectorWheel(final long animationDuration) {
        mDimSelectorWheelAnimator.setDuration(animationDuration);
        mDimSelectorWheelAnimator.start();
    }

    //--------------------------------------------------------------------------
    //
    //  Layout measuring
    //
    //--------------------------------------------------------------------------

    /**
     * Perform layout measurements
     */
    abstract protected void measureLayout();


    //--------------------------------------------------------------------------
    //
    //  Drawing stuff
    //
    //--------------------------------------------------------------------------

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mViewAdapter != null && mViewAdapter.getItemsCount() > 0) {
            if (rebuildItems()) measureLayout();

            doItemsLayout();
            drawItems(canvas);
        }
    }

    /**
     * Draws items on specified canvas
     *
     * @param canvas the canvas for drawing
     */
    abstract protected void drawItems(Canvas canvas);
}
