/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.spinnerwheel;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.animation.Interpolator;
import android.widget.Scroller;

/**
 * Scroller class handles scrolling events and updates the spinnerwheel
 */
public abstract class WheelScroller {
    /**
     * Scrolling mListener interface
     */
    public interface ScrollingListener {
        /**
         * Scrolling callback called when scrolling is performed.
         * @param distance the distance to scroll
         */
        void onScroll(int distance);

        /**
         * This callback is invoked when mScroller has been touched
         */
        void onTouch();

        /**
         * This callback is invoked when touch is up
         */
        void onTouchUp();

        /**
         * Starting callback called when scrolling is started
         */
        void onStarted();

        /**
         * Finishing callback called after justifying
         */
        void onFinished();

        /**
         * Justifying callback called to justify a view when scrolling is ended
         */
        void onJustify();
    }

    /** Scrolling duration */
    private static final int SCROLLING_DURATION = 400;

    private static final float FLING_REDUCTION = 0.4F;

    /** Minimum delta for scrolling */
    public static int MIN_DELTA_FOR_SCROLLING = -1;

    // Listener
    private ScrollingListener mListener;

    // Context
    private Context mContext;

    // Scrolling
    private GestureDetector mGestureDetector;
    protected Scroller mScroller;
    private int mLastScrollPosition;
    private float mLastTouchedPosition;
    private boolean mIsScrollingPerformed;

    /**
     * Constructor
     * @param context the current mContext
     * @param listener the scrolling mListener
     */
    public WheelScroller(final Context context, final ScrollingListener listener) {
        if (MIN_DELTA_FOR_SCROLLING == -1) {
            MIN_DELTA_FOR_SCROLLING = ViewConfiguration.get(context).getScaledTouchSlop();
        }

        mGestureDetector = new GestureDetector(context, new SimpleOnGestureListener() {
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                // Do scrolling in onTouchEvent() since onScroll() are not call immediately
                //  when user touch and move the spinnerwheel
                return true;
            }

            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                mLastScrollPosition = 0;
                scrollerFling(mLastScrollPosition, Math.round(velocityX * FLING_REDUCTION),
                              Math.round(velocityY * FLING_REDUCTION));
                setNextMessage(MESSAGE_SCROLL);
                return true;
            }

            // public boolean onDown(MotionEvent motionEvent);

        });
        mGestureDetector.setIsLongpressEnabled(false);

        mScroller = new Scroller(context);

        mListener = listener;
        mContext = context;
    }

    /**
     * Set the the specified scrolling interpolator
     * @param interpolator the interpolator
     */
    public void setInterpolator(final Interpolator interpolator) {
        mScroller.forceFinished(true);
        mScroller = new Scroller(mContext, interpolator);
    }

    /**
     * Scroll the spinnerwheel
     * @param distance the scrolling distance
     * @param time the scrolling duration
     */
    public void scroll(final int distance, final int time) {
        mScroller.forceFinished(true);
        mLastScrollPosition = 0;
        scrollerStartScroll(distance, time != 0 ? time : SCROLLING_DURATION);
        setNextMessage(MESSAGE_SCROLL);
        startScrolling();
    }

    /**
     * Stops scrolling
     */
    public void stopScrolling() {
        mScroller.forceFinished(true);
    }

    /**
     * Handles Touch event
     * @param event the motion event
     * @return
     */
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                mLastTouchedPosition = getMotionEventPosition(event);
                mScroller.forceFinished(true);
                clearMessages();
                mListener.onTouch();
                break;

            case MotionEvent.ACTION_UP:
                if (mScroller.isFinished())
                    mListener.onTouchUp();
                break;


            case MotionEvent.ACTION_MOVE:
                // perform scrolling
                int distance = (int)(getMotionEventPosition(event) - mLastTouchedPosition);
                if (distance != 0) {
                    startScrolling();
                    mListener.onScroll(distance);
                    mLastTouchedPosition = getMotionEventPosition(event);
                }
                break;
        }

        if (!mGestureDetector.onTouchEvent(event) && event.getAction() == MotionEvent.ACTION_UP) {
            justify();
        }

        return true;
    }


    // Messages
    private final int MESSAGE_SCROLL = 0;
    private final int MESSAGE_JUSTIFY = 1;

    /**
     * Set next message to queue. Clears queue before.
     *
     * @param message the message to set
     */
    private void setNextMessage(final int message) {
        clearMessages();
        animationHandler.sendEmptyMessage(message);
    }

    /**
     * Clears messages from queue
     */
    private void clearMessages() {
        animationHandler.removeMessages(MESSAGE_SCROLL);
        animationHandler.removeMessages(MESSAGE_JUSTIFY);
    }

    // animation handler
    private Handler animationHandler = new Handler() {
        public void handleMessage(Message msg) {
            mScroller.computeScrollOffset();
            int currPosition = getCurrentScrollerPosition();
            int delta = mLastScrollPosition - currPosition;
            mLastScrollPosition = currPosition;
            if (delta != 0) {
                mListener.onScroll(delta);
            }

            // scrolling is not finished when it comes to final Y
            // so, finish it manually
            if (Math.abs(currPosition - getFinalScrollerPosition()) < MIN_DELTA_FOR_SCROLLING) {
                // currPosition = getFinalScrollerPosition();
                mScroller.forceFinished(true);
            }
            if (!mScroller.isFinished()) {
                animationHandler.sendEmptyMessage(msg.what);
            } else if (msg.what == MESSAGE_SCROLL) {
                justify();
            } else {
                finishScrolling();
            }
        }
    };

    /**
     * Justifies spinnerwheel
     */
    private void justify() {
        mListener.onJustify();
        setNextMessage(MESSAGE_JUSTIFY);
    }

    /**
     * Starts scrolling
     */
    private void startScrolling() {
        if (!mIsScrollingPerformed) {
            mIsScrollingPerformed = true;
            mListener.onStarted();
        }
    }

    /**
     * Finishes scrolling
     */
    protected void finishScrolling() {
        if (mIsScrollingPerformed) {
            mListener.onFinished();
            mIsScrollingPerformed = false;
        }
    }

    protected abstract int getCurrentScrollerPosition();

    protected abstract int getFinalScrollerPosition();

    protected abstract float getMotionEventPosition(MotionEvent event);

    protected abstract void scrollerStartScroll(int distance, int time);

    protected abstract void scrollerFling(int position, int velocityX, int velocityY);
}
