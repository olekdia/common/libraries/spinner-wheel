/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.spinnerwheel;

/**
 * Wheel insert listener interface.
 * <p>The onWheelInsert() method is called whenever new value was inserted to adapter by EditText
 */
public interface OnWheelInsertListener {
	/**
	 * Callback method to be invoked when new value was inserted
	 * @param wheel the spinnerwheel view whose state has changed
	 */
	void onWheelInsert(AbstractWheel wheel);
}
