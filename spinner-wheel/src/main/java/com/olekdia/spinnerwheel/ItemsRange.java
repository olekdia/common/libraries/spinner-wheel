/*
 * android-spinnerwheel
 * https://github.com/ai212983/android-spinnerwheel
 *
 * based on
 *
 * Android Wheel Control.
 * https://code.google.com/p/android-wheel/
 *
 * Copyright 2011 Yuri Kanivets
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.spinnerwheel;

/**
 * Range for visible items.
 */
public class ItemsRange {

    // First item number
    private int mFirst;

    // Items mCount
    private int mCount;

    /**
     * Default constructor. Creates an empty range
     */
    public ItemsRange() {
        this(0, 0);
    }

    /**
     * Constructor
     * @param first the number of mFirst item
     * @param count the mCount of items
     */
    public ItemsRange(final int first, final int count) {
        mFirst = first;
        mCount = count;
    }

    public final void set(final int first, final int count) {
        mFirst = first;
        mCount = count;
    }

    /**
     * Gets number of  mFirst item
     * @return the number of the mFirst item
     */
    public final int getFirst() {
        return mFirst;
    }

    /**
     * Gets number of last item
     * @return the number of last item
     */
    public final int getLast() {
        return mFirst + mCount - 1;
    }

    /**
     * Get items mCount
     * @return the mCount of items
     */
    public final int getCount() {
        return mCount;
    }

    /**
     * Tests whether item is contained by range
     * @param index the item number
     * @return true if item is contained
     */
    public final boolean contains(final int index) {
        return index >= mFirst && index <= getLast();
    }
}