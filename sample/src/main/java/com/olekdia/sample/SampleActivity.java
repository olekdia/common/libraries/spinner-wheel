package com.olekdia.sample;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.olekdia.spinnerwheel.WheelHorizontalView;
import com.olekdia.spinnerwheel.WheelVerticalView;
import com.olekdia.spinnerwheel.adapters.ArrayWheelAdapter;
import com.olekdia.spinnerwheel.adapters.ArrayWheelAddableAdapter;
import com.olekdia.spinnerwheel.adapters.ArrayWheelAddableInIntervalAdapter;
import com.olekdia.spinnerwheel.adapters.NumericWheelAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class SampleActivity extends AppCompatActivity implements View.OnClickListener {

    public static String[] CITIES = new String[] {
        "Kiev", "New-York", "London", "Amsterdam", "Moscow", "Rome"
    };

    public static String[] COUNTRIES = new String[] {
        "Ukraine", "USA", "Great Britain", "Holland", "Russian", "Italy"
    };

    public static final Integer[] MSEC_VALUES_INT = new Integer[]{
            0, 100, 200, 300, 400, 500, 600, 700, 800, 900
    };

    public static ArrayList<Integer> MSEC_VALUES_LIST = new ArrayList<>(Arrays.asList(MSEC_VALUES_INT));
    public static ArrayList<String> CITIES_LIST = new ArrayList<>(Arrays.asList(CITIES));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        findViewById(R.id.container).setOnClickListener(this);

        final Resources res = getResources();
        final NumericWheelAdapter adapter = new NumericWheelAdapter(this, 0, 90);
        adapter.setTextSize(res.getDimensionPixelSize(R.dimen.wheel_font_size));
        adapter.setTextColor(0xFF727272);
        adapter.setTextVertPadding(res.getDimensionPixelSize(R.dimen.wheel_item_padding));

        final WheelVerticalView inhalePicker = findViewById(R.id.inhale_picker);
        inhalePicker.setViewAdapter(adapter);

        final WheelVerticalView retainPicker = findViewById(R.id.retain_picker);
        retainPicker.setViewAdapter(adapter);

        final NumericWheelAdapter adapterExhale = new NumericWheelAdapter(this, 2, 20);
        adapterExhale.setTextSize(adapter.getTextSize());
        adapterExhale.setTextColor(adapter.getTextColor());
        adapterExhale.setTextVertPadding(adapter.getTextVertPadding());

        final WheelVerticalView exhalePicker = findViewById(R.id.exhale_picker);
        exhalePicker.setViewAdapter(adapterExhale);

        final WheelVerticalView sustainPicker = findViewById(R.id.sustain_picker);
        sustainPicker.setViewAdapter(adapter);
        sustainPicker.setIsInputEnabled(false);

        // Millis
        final ArrayWheelAddableInIntervalAdapter millisAdapter = new ArrayWheelAddableInIntervalAdapter(this, MSEC_VALUES_LIST, 0, 999);
        millisAdapter.setTextSize(adapter.getTextSize());
        millisAdapter.setTextColor(adapter.getTextColor());
        millisAdapter.setTextVertPadding(adapter.getTextVertPadding());

        final WheelVerticalView millisPicker = findViewById(R.id.millis_picker);
        millisPicker.setViewAdapter(millisAdapter);
        millisPicker.setCurrentItem(2);

        // Cities
        final ArrayWheelAddableAdapter cityAdapter = new ArrayWheelAddableAdapter(this, CITIES_LIST, true);
        cityAdapter.setTextSize(adapter.getTextSize());
        cityAdapter.setTextColor(adapter.getTextColor());
        cityAdapter.setTextVertPadding(adapter.getTextVertPadding());

        final WheelVerticalView cityPicker =  findViewById(R.id.city_picker);
        cityPicker.setViewAdapter(cityAdapter);
        cityPicker.setCurrentItem(2);

        // Countries
        final ArrayWheelAdapter countryAdapter = new ArrayWheelAdapter(this, COUNTRIES);
        countryAdapter.setTextSize(adapter.getTextSize());
        countryAdapter.setTextColor(adapter.getTextColor());
        countryAdapter.setTextVertPadding(adapter.getTextVertPadding());

//        final WheelHorizontalView countryPicker = findViewById(R.id.country_picker);
//        countryPicker.setViewAdapter(countryAdapter);
//        countryPicker.setCurrentItem(2);

        // Ten
        final NumericWheelAdapter tenAdapter = new NumericWheelAdapter(this, 0, 9);
        tenAdapter.setTextSize(adapter.getTextSize());
        tenAdapter.setTextColor(adapter.getTextColor());
        tenAdapter.setTextVertPadding(adapter.getTextVertPadding());

        final WheelHorizontalView tenPicker = findViewById(R.id.ten_picker);
        tenPicker.setViewAdapter(tenAdapter);
        tenPicker.setCurrentItem(2);
    }

    @Override
    public void onClick(View v) {
        v.requestFocus();
        Log.d("request", "focus");
    }
}
